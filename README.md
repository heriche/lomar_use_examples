## LOMAR_use_examples

Code notebooks used to generate material for the figures in the paper describing the [LOMAR package](https://git.embl.de/heriche/lomar).

## How to run

* Clone the project:

``` 
git clone https://git.embl.de/heriche/lomar_use_examples.git
```
 or download it from:
 
 https://git.embl.de/heriche/lomar_use_examples/-/archive/main/lomar_use_examples-main.zip
 
 and uncompress it.
 
 * Open one of the notebooks with [RStudio](https://www.rstudio.com/products/rstudio/download/)
 * Make sure the working directory is set to the project directory either using the menu Session > Set Working Directory > Choose Directory and then selecting the project directory or using the RStudio console:
 ```
 setwd("/path/to/lomar_use_examples")
 ```
 * Click the 'Run > Run All' button (or from the menu select Code > Run Region > Run All)
