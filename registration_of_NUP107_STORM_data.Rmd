---
title: "Joint registration of NUP107 from Heydarian et al."
output: html_notebook
---

```{r}
library(LOMAR)
library(dbscan)
library(rgl)
library(RColorBrewer)
library(pals)
library(htmlwidgets)
library(htmltools)
library(tictoc)

set.seed(256936)

output.dir <- "output"
dir.create(output.dir)

wd <- getwd()

# RGL visualization parameters
userMatrix <- diag(1, 4) # view along z axis (top down)
windowRect <- c(0,0,500,500) # window size

plots <- list()

# Read the data,
# It has been converted to an R data frame from the matlab file available here:
# https://data.4tu.nl/ndownloader/files/26404670
PS <- readRDS("data/NUP107_STORM.rds")

C <- locprec2cov(PS)
PS <- lapply(PS, function(x) { x[, c("x", "y", "z")] })

tic("Joint registration...")
tr <- jrmpc(V = PS, C = C, tol = 0.01, maxIter = 200, fixedVarIter = 5, K = 100, initializeBy = 'sampling', model.selection = TRUE, model.selection.threshold = 0.01)
toc()
registered.PS <- do.call(rbind, tr$Y)

I <- as.data.frame(registered.PS * 120)
colnames(I) <- c("x", "y", "z")
# Crop
mu <- colMeans(I)
I <- I[I$x>(mu[1]-65) & (I$x<mu[1]+65) & I$y>(mu[2]-65) & (I$x<mu[2]+65),]

I$channel <- 1
I <- points2img(I, voxel.size = c(3,3,3), channel = 1, method = 'histogram')
img <- apply(I, c(1,2), sum) # 2D projection along z
img.size <- max(dim(img))
png(file.path(output.dir, "NUP107_registered.png"), 
    width = img.size*10, height = img.size*10,
    unit = "px", res = 300)
image(1:dim(img)[1], 1:dim(img)[2], img, col = gray.colors(100, start = 0), xlab = "", ylab = "")
dev.off()

# Show registered point cloud before outlier removal
# Apply local density-weighted downsampling for visualization
registered.PS <- downsample(registered.PS, n = 2000, k = 23)
open3d(userMatrix = userMatrix, windowRect = windowRect)
plot3d(registered.PS[,1], registered.PS[,2], registered.PS[,3], box = FALSE, col = 'blue', xlab = '', ylab = '', zlab = '', xlim = c(-1.1, 1.1), ylim = c(-1.1, 1.1), zlim = c(-0.7, 0.7))
title3d(main = "Joint registration of NUP107 point sets")
plots[[1]] <- rglwidget()
rgl.postscript(file = file.path(output.dir, "NUP107.top_view.pdf"), fmt = "pdf")

# Find high variance components
threshold <- quantile(tr$S, 0.75)
high.var.idx <- which(tr$S>threshold)

# Show GMM centres, highlighting those with high variance
GMM <- as.data.frame(cbind(tr$M, tr$S))
colnames(GMM) <- c("x", "y", "z", "S")
colours <- rep("blue", nrow(GMM))
colours[high.var.idx] <- "red"

open3d(userMatrix = userMatrix, windowRect = windowRect)
plot3d(GMM$x, GMM$y, GMM$z, col = colours, type = 's', size = 2, box = FALSE, 
       xlab = '', ylab = '', zlab = '', 
       xlim = c(-0.15,0.15), ylim = c(-0.15, 0.15), zlim = c(-0.15, 0.15))
title3d(main = "Registration GMM centres")
plots[[2]] <- rglwidget()
rgl.postscript(file = file.path(output.dir, "NUP107-GMM_centres.top_view.pdf"), fmt = "pdf")
rgl.viewpoint(userMatrix = rotationMatrix(-90*pi/180, 1, 0, 0))
rgl.postscript(file = file.path(output.dir, "NUP107-GMM_centres.side_view.pdf"), fmt = "pdf")
rgl.viewpoint(userMatrix = rotationMatrix(-30*pi/180, 1, 0, 0))
rgl.postscript(file = file.path(output.dir, "NUP107-GMM_centres.tilted_view.pdf"), fmt = "pdf")

# Identify and remove outliers
# Outliers are points associated with high-variance components of the model
for(i in 1:length(tr$Y)) {
  pts.to.remove <- NULL
  n <- nrow(tr$Y[[i]])
  for(j in 1:n) {
    cmp.idx <- which.max(tr$a[[i]][j,])
    if(cmp.idx %in% high.var.idx) {
      pts.to.remove <- c(pts.to.remove, j)
    }
  }
  l <- length(pts.to.remove)
  if(l>0 && l<n) {
    tr$Y[[i]] <- tr$Y[[i]][-pts.to.remove,]
  } else if (l == n) {
    tr$Y[[i]] <- NULL
  }
}
registered.PS <- do.call(rbind, tr$Y)

I <- as.data.frame(registered.PS * 120)
colnames(I) <- c("x", "y", "z")
# Crop
mu <- colMeans(I)
I <- I[I$x>(mu[1]-65) & (I$x<mu[1]+65) & I$y>(mu[2]-65) & (I$x<mu[2]+65),]
I$channel <- 1
I <- points2img(I, voxel.size = c(3,3,3), channel = 1, method = 'histogram')
img <- apply(I, c(1,2), sum) # 2D projection along z
img.size <- max(dim(img))
png(file.path(output.dir, "NUP107_registered-outliers_removed.png"), 
    width = img.size*10, height = img.size*10,
    unit = "px", res = 300)
image(1:dim(img)[1], 1:dim(img)[2], img, col = gray.colors(100, start = 0), xlab = "", ylab = "")
dev.off()

# Denoising with DBSCAN
registered.PS <- downsample(registered.PS, n = 2000, k = 23)
# kNNdistplot(registered.PS, k = 33) # Use to visually find eps
eps <- find_elbow(sort(kNNdist(registered.PS, k = 33)))[2]
dbr <- dbscan(registered.PS, minPts = 33, eps = eps)
registered.PS <- registered.PS[-which(dbr$cluster == 0),] # DBSCAN cluster 0 is noise

I <- as.data.frame(registered.PS * 120)
colnames(I) <- c("x", "y", "z")
I$channel <- 1
I <- points2img(I, voxel.size = c(3,3,3), channel = 1, method = 'histogram')
img <- apply(I, c(1,2), sum) # 2D projection along z
img.size <- max(dim(img))
png(file.path(output.dir, "NUP107_registered-denoised.png"), 
    width = img.size*10, height = img.size*10,
    unit = "px", res = 300)
image(1:dim(img)[1], 1:dim(img)[2], img, col = gray.colors(100, start = 0), xlab = "", ylab = "")
dev.off()

# Show registered point cloud after noise and outlier removal
open3d(userMatrix = userMatrix, windowRect = windowRect)
plot3d(registered.PS[,1], registered.PS[,2], registered.PS[,3], box = FALSE, col = 'blue', xlab = '', ylab = '', zlab = '', xlim = c(-1.1,1.1), ylim = c(-1.1, 1.1), zlim = c(-0.7, 0.7))
title3d(main = "NUP107, outliers and noise removed")
plots[[3]] <- rglwidget()
rgl.postscript(file = file.path(output.dir, "NUP107-denoised.top_view.pdf"), fmt = "pdf")
rgl.viewpoint(userMatrix = rotationMatrix(-90*pi/180, 1, 0, 0))
rgl.postscript(file = file.path(output.dir, "NUP107-denoised.side_view.pdf"), fmt = "pdf")

# save_html can only write to current directory unless a full path is used
setwd(output.dir)
htmltools::save_html(plots, file = "NUP107-STORM.html")
setwd(wd)

```

